import java.util.*

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)
    println("Introduzca su dni para saber la letra: ")
    val dni = scanner.nextInt()

    val numeros = arrayOf("T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E",)

    println("La letra de su dni es: ${dni}${numeros[dni%23]}")

}