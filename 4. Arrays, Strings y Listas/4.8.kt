import java.util.*

fun main(args: Array<String>){

    var count = 0
    val last = args.last()

    for (i in 0 until args.lastIndex){
        if (args[i] == last)
            ++count
    }
    println("El ultimo número es $last y tiene $count números repetidos.")
}