import java.util.*

fun main(args: Array<String>){

    var parells = 0
    var senars = 0

    for (i in args){
        if (i.toInt() %2 == 0) parells++
        else senars++
    }
    println("Parells: $parells")
    println("Senars: $senars")
}
