import java.util.*

/*
* AUTHOR: Name Surname1 Surname2
* DATE: year/moth/day
* TITLE: Parèntesis
*/


/*
'()' => true
 */

fun main(){

    //VARIABLES
    val scanner = Scanner(System.`in`)

    //test1:  '(())()()'
    //test2: '(()()))('

    var list = mutableListOf("")
    val input: String = scanner.nextLine()


}





/*
PLANTEAMIENTO:

- Compruebo si el caracter es de cierre o apertura.
- Si el caracter es de apertura se guarda en una lista mutable.
- Si el caracter es de cierre lo comparamos con el último valor de la lista.
- Si son iguales continuo.

    //test2: '(()()))('

1. El primer caracter es "(". Como es de apertura lo guardo en la lista mutable.
2. El segundo caracter es "(". Como es de apertura lo guardo en la lista mutable.
3. El tercer caracter es ")". Como es de cierre lo comparo. Como

 */