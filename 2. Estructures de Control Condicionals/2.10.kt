package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    println("Introduzca el DNI sin letra: ")
    val input = scanner.nextInt()
    val calculo = input % 23

    // ASIGNACIÓN DE LETRAS A NUMEROS

    if (calculo == 0) println("$input T")
    else if (calculo == 1) println("$input R")
    else if (calculo == 2) println("$input W")
    else if (calculo == 3) println("$input A")
    else if (calculo == 4) println("$input G")
    else if (calculo == 5) println("$input M")
    else if (calculo == 6) println("$input Y")
    else if (calculo == 7) println("$input F")
    else if (calculo == 8) println("$input P")
    else if (calculo == 9) println("$input D")
    else if (calculo == 10) println("$input X")
    else if (calculo == 11) println("$input B")
    else if (calculo == 12) println("$input N")
    else if (calculo == 13) println("$input J")
    else if (calculo == 14) println("$input Z")
    else if (calculo == 15) println("$input S")
    else if (calculo == 16) println("$input Q")
    else if (calculo == 17) println("$input V")
    else if (calculo == 18) println("$input H")
    else if (calculo == 19) println("$input L")
    else if (calculo == 20) println("$input C")
    else if (calculo == 21) println("$input K")
    else if (calculo == 22) println("$input E")

}