package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    println("Inserte los números: ")
    val input = scanner.nextInt()
    val input2 = scanner.nextInt()
    val caracter = scanner.next()

    if (caracter == "+") println(input + input2)
    else if (caracter == "-") println(input - input2)
    else if (caracter == "*") println(input * input2)
    else if (caracter == "/") println(input / input2)
    else if (caracter == "%") println(input % input2)
}