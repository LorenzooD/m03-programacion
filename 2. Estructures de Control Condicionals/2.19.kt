package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a numbers : ")
    val number = scanner.nextDouble()

    if (number == 0.0) println("Nota inválida")
    else if (number in 0.1 .. 4.9) println("Insuficiente")
    else if (number in 5.0 .. 6.9) println("Suficiente")
    else if (number in 7.0 .. 9.9) println("Notable")
    else if (number == 10.0 ) println("Excelente")

}