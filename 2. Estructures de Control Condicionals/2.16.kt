package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a number: ")
    val number = scanner.nextInt()

    when (number){
        1, 3, 5, 7, 8, 10, 12 -> println("31")
        2 -> println("28 días o 29 en año bisiesto")
        else -> println("30")
    }




}