package Conditions


import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    println("Insert the first number: ")
    val num1 = scanner.nextInt()

    println("Insert the second number: ")
    val num2 = scanner.nextInt()

    println("Insert the thirst number: ")
    val num3 = scanner.nextInt()

    if (num1 > num2 && num1 > num3) println("$num1 es el más grande")
    else if (num2 > num1 && num2 > num3) println("$num2 es el más grande")
    else if (num3 > num1 && num3 > num2) println("$num3 es el más grande")
    else println("Los 3 números son iguales")
}
