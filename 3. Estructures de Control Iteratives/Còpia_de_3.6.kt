package Bucles

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Inserta el numero: ")
    val base = scanner.nextInt()
    print("Numero a elevar: ")
    var exponent = scanner.nextInt()
    var result: Long = 1

    while (exponent != 0) {
        result *= base.toLong()
        --exponent
    }

    println("La respuesta es = $result")
}
