import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    val numAleatorio = (1..100).random()

    do {
        println("Introduzca un valor: ")
        val num = scanner.nextInt()

        if (num > numAleatorio){
            print("Demasiado alto. ")
        }
        else if (num < numAleatorio){
            print("Demasiado bajo. ")
        }
        else if (num == numAleatorio){
            print("¡Numero correcto!")
        }
    } while (num != numAleatorio )


}