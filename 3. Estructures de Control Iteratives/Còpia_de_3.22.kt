import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    println("Introduce un valor: ")
    val lineas = scanner.nextInt()

    for (i in 1 until lineas) {
        for(k in i..(lineas-1))
        {
            print(" ")
        }

        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }

    for (i in lineas downTo 1) {
        for(k in i..(lineas-1))
        {
            print(" ")
        }

        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }
}