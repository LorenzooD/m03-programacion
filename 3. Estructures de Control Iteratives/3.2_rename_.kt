package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    println("Ingrese el número: ")
    val num = scanner.nextInt()

    var resultado = 0

    for (i in 1..num){
        resultado += i

    }
    println("El resultado es $resultado")

}