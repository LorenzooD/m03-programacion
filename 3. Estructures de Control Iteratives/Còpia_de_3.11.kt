import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    val num = scanner.nextInt()
    var result = 0.0

    for (i in 1..num){
        result = 1.0 / i + result
    }
    println("La respuesta es: $result")
}