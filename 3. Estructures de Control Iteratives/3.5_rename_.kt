package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    print("Inserta la base: ")
    val base = scanner.nextInt()
    print("Inserta el numero a elevar: ")
    var exp = scanner.nextInt()

    var resultado: Long = 1

    while (exp != 0){
        resultado *=base
        --exp
    }
    println(resultado)
}