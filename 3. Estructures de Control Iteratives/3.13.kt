import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    val num = scanner.nextInt()
    var result = 0.0

    for (i in 1..num){
        if (1 % 2 != 0) {
            result = 1.0 /i - result
        }
        else if (1 % 2 == 0){
            result += i / 1.0
        }
    }
    println("La respuesta es: $result")
}