import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    val numAleatorio = (1..100).random()
    var intentos = 0


    do {
        println("Introduzca un valor: ")
        val num = scanner.nextInt()

        if (num > numAleatorio){
            print("Demasiado alto. ")
            intentos++
        }
        else if (num < numAleatorio){
            print("Demasiado bajo. ")
            intentos++
        }
        else if (num == numAleatorio){
            print("¡Numero correcto!")
            intentos++
        }
        if (intentos == 6){
            print("¡¡HAS PERDIDO!!")
        }
    } while (intentos < 6 )


}