import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    val num = scanner.nextInt()
    var factorial = 1

    for (i in 1.. num){
        factorial *= i
    }
    println(factorial)
}