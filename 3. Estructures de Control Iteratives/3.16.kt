import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var x = 0
    var y = 0

    do {
        println("Introduzca un valor: ")
        val coords = scanner.next().single()
        if (coords == 'n'){
            y--
        }
        if (coords == 's'){
            y++
        }
        if (coords == 'e'){
            x--
        }
        if (coords == 'o'){
            x++
        }
    }while (coords != 'z')
    println("($x, $y)")
}