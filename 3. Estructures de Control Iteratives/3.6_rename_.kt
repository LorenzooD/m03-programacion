package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    print("Inserta una cadena de números: ")
    var number = scanner.nextInt()
    var numRev = 0

    while (number > 0) {
        numRev = numRev * 10 + number % 10
        number /= 10
    }
    println(numRev)
}