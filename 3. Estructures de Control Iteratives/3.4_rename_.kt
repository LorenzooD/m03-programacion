package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    println("Inserta la tabla de multiplicar: ")
    val numero = scanner.nextInt()

    var multi = 0

    for (i in 1..10){
        multi = i * numero
        println("$numero x $i = $multi")
    }
}