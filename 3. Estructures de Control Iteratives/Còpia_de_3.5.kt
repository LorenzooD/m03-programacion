package Bucles

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    println("Elige la tabla de multiplicar:  ")
    val number = scanner.nextInt()

    for (i in 1..10) {
        val producto = number * i
        println("$number * $i = $producto")
    }
}