package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    println("Inserte el primer número: ")
    val numero1 = scanner.nextInt()
    println("Inserte el segundo número: ")
    val numero2 = scanner.nextInt()

    for (i in numero1 until  numero2) print("$i, ")
}