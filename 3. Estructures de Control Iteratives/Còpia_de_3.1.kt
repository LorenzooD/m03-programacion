package Bucles
/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 06/09/2022
* TITLE: Pinta X Números
*/

import java.util.*

fun main() {

    //Creamos la caja
    val scanner = Scanner(System.`in`)

    println("Insert a number: ")
    //Valor que introduciremos
    val input = scanner.nextInt()

    // i es una "variable por defecto que se le asigna al bucle for"
    for (i in 1..input){
        println(i)
    }
}
