package Conditions

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)

    print("Inserta una cadena de números: ")
    var number = scanner.nextInt()
    var suma = 0L

    while (number != 0){
        number /= 10
        suma++
    }
    println("Tiene $suma digitos")
}

// El programa cuenta cuantas veces divides el numero insertado entre 10 y lo almacena en la variable "suma".