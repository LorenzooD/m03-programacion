import java.util.*
import kotlin.math.PI

fun main() {

    val scanner = Scanner(System.`in`)

    println("Introduzca un número: ")
    val diametro = scanner.nextDouble()

    val PI = PI

    val superficie = PI * (diametro / 2) * (diametro / 2)

    println(superficie)


}

/*
Lo primero que hacemos es crear la variable PI = (3.14..)
Después multiplicamos PI por el diametro y lo dividimos entre 2. Así 2 veces. Sería como elevarlo a 2
 */