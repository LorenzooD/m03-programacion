import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    println("Introduzca la temperatura: ")
    val celsius = scanner.nextDouble()

    val fahrenheit = (celsius * 9/5) + 32

    println("La temperatura a fahrenheit es de: $fahrenheit")

}